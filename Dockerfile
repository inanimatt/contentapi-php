FROM tuimedia/base:alpine

MAINTAINER Matt Robinson <matt@tuimedia.com>

EXPOSE 80

COPY . /var/www/app
WORKDIR /var/www/app

CMD ["docker/run.sh"]
