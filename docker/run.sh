#!/bin/bash
echo "Preparing configuration"

# Required, can't be empty
: "${DOCUMENT_ROOT:?Need to set DOCUMENT_ROOT}"
: "${INDEX_FILE:?Need to set INDEX_FILE}"
: "${APP_ENTRYPOINT:?Need to set APP_ENTRYPOINT}"
: "${SERVER_NAME:?Need to set SERVER_NAME}"
: "${APP_ENV:?Need to set APP_ENV}"
: "${NGINX_CERT:?Need to set NGINX_CERT}"
: "${NGINX_KEY:?Need to set NGINX_KEY}"
: "${SYMFONY__DATABASE_HOST:?Need to set SYMFONY__DATABASE_HOST}"
: "${SYMFONY__DATABASE_NAME:?Need to set SYMFONY__DATABASE_NAME}"
: "${SYMFONY__DATABASE_USER:?Need to set SYMFONY__DATABASE_USER}"
: "${SYMFONY__DATABASE_PASSWORD:?Need to set SYMFONY__DATABASE_PASSWORD}"
: "${GITHUB_OAUTH_TOKEN:?Need to set GITHUB_OAUTH_TOKEN}"

mkdir -p /etc/nginx/ssl /etc/supervisor.d
cp -v /var/www/app/docker/php-app.ini /etc/php7/conf.d/99_custom_app.ini
cp -v /var/www/app/docker/supervisord.conf /etc/supervisord.conf
cp -v /var/www/app/docker/supervisor-docker.ini /etc/supervisor.d/
cp -v /var/www/app/docker/nginx.conf /etc/nginx/nginx.conf
cp -v /var/www/app/docker/nginx-app.conf /etc/nginx/conf.d/nginx-app.conf
cp -v /var/www/app/docker/self_cert.config /etc/nginx/ssl/self_cert.config

echo "Configuring web server"
env | grep ^SYMFONY__ | sed -E "s/^(SYMFONY__[^=]+)=(.+)/fastcgi_param \1 \"\2\";/g" >> /etc/nginx/fastcgi_params

sed -i "s|DOCUMENT_ROOT|$DOCUMENT_ROOT|g;s|INDEX_FILE|$INDEX_FILE|g;s|APP_ENTRYPOINT|$APP_ENTRYPOINT|g;s|SERVER_NAME|$SERVER_NAME|g;s|NGINX_CERT|$NGINX_CERT|g;s|NGINX_KEY|$NGINX_KEY|g;s|NGINX_APP_ENV|$APP_ENV|g" /etc/nginx/conf.d/nginx-app.conf

echo "Creating folders"
mkdir -p \
  /var/www/app/var/cache/dev \
  /var/www/app/var/cache/prod \
  /var/www/app/var/logs \
  /var/www/app/var/sessions

echo "Saving environment"
env > /var/www/app/app/config/env

echo "Checking composer dependencies"
cd /var/www/app
composer config -g github-oauth.github.com $GITHUB_OAUTH_TOKEN
composer install -n

echo "Waiting for database"
while ! nc -z $SYMFONY__DATABASE_HOST 3306; do sleep 1; done

echo "Migrating database"
bin/console doc:mig:mig -n

echo "Setting cache and log permissions"
sudo -u nginx bin/console cache:clear -e prod

chown -R nginx:nginx \
    /var/www/app/var/cache \
    /var/www/app/var/logs \
    /var/www/app/var/sessions

chmod -R g+wX \
    /var/www/app/var/cache \
    /var/www/app/var/logs \
    /var/www/app/var/sessions

echo "Starting supervisor"
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
